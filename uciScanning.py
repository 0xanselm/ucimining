import json
import urllib.parse
import urllib.request
import uciExport
import uciMining
import uciRequestHeaders

HEADERS = {'User-Agent': 'Mozilla/5.0'}
HOST = 'https://dataride.uci.org'
OVERVIEW_PAGE = '/iframe/Competitions/'
RACES = '/iframe/Races/'
EVENTS = '/iframe/Events/'
RESULTS = '/iframe/Results/'


def scanRaces():
    # scanning for races aka stages
    data = urllib.parse.urlencode(uciRequestHeaders.RACES)
    data = data.encode('utf-8')
    req = urllib.request.Request(HOST+RACES, data, headers=HEADERS)
    resp = urllib.request.urlopen(req)
    respData = resp.read()
    respJson = json.loads(respData.decode('utf-8'))
    return respJson


def scanEvents(dirName, racesID):
    # scanning for events likes Stage Class, Mountain Class, Points Class...
    eventsID = []
    for i in racesID:
        uciRequestHeaders.EVENTS['raceId'] = i
        data = urllib.parse.urlencode(uciRequestHeaders.EVENTS)
        data = data.encode('utf-8')
        req = urllib.request.Request(HOST+EVENTS, data, headers=HEADERS)
        resp = urllib.request.urlopen(req)
        respData = resp.read()
        respJson = json.loads(respData.decode('utf-8'))
        eventsID.append(respJson)
        # uciExport.exportJson(dirName, str(i)+".json", respJson)
    return eventsID


def scanResults(dirName, eventsID):
    '''
    The TdF f.e. consists of 21 Races
    Each Race consists of 6 Events.
    For each Event we have a Ranking
    We get the ranking from eventsNUMBER.json.EventID
    '''
    stage = len(eventsID)  # event equals stage
    for race in eventsID:
        for event in race:
            # There seems to be an empyt 'data' field in event json
            if event == 'data':
                break
            if uciMining.checkForExistingFiles(dirName, event):
                continue
            print("Working on stage", stage,
                  event['EventName'], event['EventId'])
            uciRequestHeaders.RESULTS['eventId'] = event['EventId']
            data = urllib.parse.urlencode(uciRequestHeaders.RESULTS)
            data = data.encode('utf-8')
            req = urllib.request.Request(HOST+RESULTS, data, headers=HEADERS)
            resp = urllib.request.urlopen(req)
            respData = resp.read()
            respJson = json.loads(respData.decode('utf-8'))
            eventsID.append(respJson)

            jsonFileName = 'Stage' + \
                str(stage).rjust(2, "0") + " " + \
                str(event['EventName']) + " " + str(event['EventId']) + '.json'
            uciExport.exportJson(dirName, jsonFileName, respJson)
        stage -= 1
