import uciMining
import uciScanning
import uciExport

'''
https://dataride.uci.org/iframe/CompetitionResults/62623?disciplineId=10
Races are the Stages
Events are for every Stage (Stages Class, Mountain, Points)
The ID in Races is the raceID in Events
The eventID gives the individual Classfifications
'''

RESULTS_DIR = './results'

def main():
    racesJson = uciScanning.scanRaces()
    racesID = uciMining.mineRaces(racesJson)
    eventsID = uciScanning.scanEvents(RESULTS_DIR, racesID)
    uciScanning.scanResults(RESULTS_DIR, eventsID)
    uciExport.loadJson(RESULTS_DIR)
    uciExport.cleanDir(RESULTS_DIR)

if __name__ == "__main__":
    main()
