RACES = {
    'disciplineId': '9',
    'competitionId': '62623',
    'take': '40',
    'skip':  '0',
    'page':  '1',
    'pageSize':  '40',
}

EVENTS = {
    'disciplineId': '9',
    'raceId': ''
}

RESULTS = {
    'disciplineId': '9',
    'eventId': '',
    'take': '250',
    'skip': '0',
    'page': '1',
    'pageSize': '250',
}
