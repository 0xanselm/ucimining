import errno
import json
import os
import glob
import pandas as pd


def loadJson(dirName):
    relevantJsons = glob.glob(os.path.join(dirName, '*.json'))
    [exportExcelPointsStages(dirName, j) if "Overall Points Classification" in j or "Overall Mountain Classification" in j else exportExcelTimeStages(
        dirName, j) for j in relevantJsons]


def exportJson(dirName, jsonFileName, dataJson):
    tempDirName = dirName.replace('./', '')
    try:
        os.makedirs(tempDirName)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise
    with open(os.path.join(dirName, jsonFileName), 'w') as f:
        json.dump(dataJson, f, indent=4)


def exportExcelTimeStages(dirName, jsonFilename, dataJson=""):
    outputName = os.path.splitext(os.path.basename(jsonFilename))[0]
    outputPath = os.path.join(dirName, outputName)
    with open(jsonFilename, 'r') as f:
        data = json.loads(f.read())
    data = data['data']
    df = pd.DataFrame(data)
    columns = ['ResultId', 'Rank', 'Age', 'IndividualDisplayName',
               'TeamName', 'NationName', 'ResultValue']
    df[columns].to_excel(outputPath+'.xlsx')


def exportExcelPointsStages(dirName, jsonFilename, dataJson=""):
    outputName = os.path.splitext(os.path.basename(jsonFilename))[0]
    outputPath = os.path.join(dirName, outputName)
    with open(jsonFilename, 'r') as f:
        data = json.loads(f.read())
    data = data['data']
    df = pd.DataFrame(data)
    columns = ['ResultId', 'Bib', 'Age', 'IndividualDisplayName',
               'TeamName', 'NationName', 'ResultValue']
    df[columns].to_excel(outputPath+'.xlsx')


def cleanDir(dirName):
    relevantJsons = glob.glob(os.path.join(dirName, '*.json'))
    for j in relevantJsons:
        os.remove(j)
