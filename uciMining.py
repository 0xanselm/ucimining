import glob
import os

def mineRaces(racesJson):
    racesID = []
    for i in racesJson['data']:
        racesID.append(i['Id'])
    return racesID

def checkForExistingFiles(dirName, event):
    '''
    A function for checking if the files on the server are
    present on the local machine, if not they will be loaded
    in uciScanning.scanResults
    '''
    for j in glob.glob(os.path.join(dirName, "*")):
        if str(event['EventId']) in j:
            return True
    return False